;;; sane-defaults.el --- sane defaults -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(advice-add #'yes-or-no-p :override #'y-or-n-p)
(add-hook 'before-save-hook
          (lambda ()
            (when buffer-file-name
              (let ((dir (file-name-directory buffer-file-name)))
                (when (and (not (file-exists-p dir))
                           (y-or-n-p (format "Directory %s does not exist. Create it?" dir)))
                  (make-directory dir t))))))
(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)
(add-hook 'prog-mode-hook 'subword-mode)
(add-to-list '+diminished-list 'subword-mode)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'eldoc-mode)
(add-hook 'ielm-mode-hook 'eldoc-mode)
(add-hook 'prog-mode-hook 'eldoc-mode)
(add-to-list '+diminished-list 'eldoc-mode)

;; fonts
(cond
 ((find-font (font-spec :name "Ubuntu Mono"))
  (set-frame-font "Ubuntu Mono-10"))
 ((find-font (font-spec :name "Fira Code"))
  (set-frame-font "Fira Code-10"))
 ((find-font (font-spec :name "Source Code Pro"))
  (set-frame-font "Source Code Pro-10"))
 ((find-font (font-spec :name "DejaVu Sans Mono"))
  (set-frame-font "DejaVu Sans Mono-10")))

;; electric-*
(with-eval-after-load 'electric
  (setq-default electric-indent-chars '(?\n ?\^?)))
(electric-pair-mode 1)
(add-function :before-until electric-pair-inhibit-predicate
              (lambda (c) (eq c ?<   ;; >
                              )))

(setq-default indent-tabs-mode nil
              tab-width 2
              tab-always-indent nil)
(setq tabify-regexp "^\t* [ \t]+")
(set-default 'indicate-empty-lines t)
(setq-default fill-column 80)
(setq-default word-wrap t)
(setq-default truncate-lines t)
(setq truncate-partial-width-windows nil)

(defun +pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command scroll-down-command
                                     recenter-top-bottom other-window))
  (advice-add command :after #'+pulse-line))
(global-auto-revert-mode 1)

(setq mouse-yank-at-point t
      global-font-lock-mode t
      y-or-n-p-use-read-key t
      require-final-newline t
      delete-selection-mode t
      transient-mark-mode t
      sentence-end-double-space nil
      vc-follow-symlinks t
      help-window-select nil
      tab-first-completion 'complete
      enable-recursive-minibuffers t
      require-final-newline t
      create-lockfiles nil
      browse-url-generic-program "xdg-open")

(customize-set-variable 'auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 0)
(customize-set-variable 'scroll-preserve-screen-position t)
(customize-set-variable 'kill-do-not-save-duplicates t)

;; org-mode
;; use external `org' package.
(+install-package 'org)
(+install-package 'org-contrib)
(require 'org)
(require 'org-eldoc)

(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)

(defun org-syntax-table-modify ()
  "Modify `org-mode-syntax-table' for the current org buffer."
  (modify-syntax-entry ?< "." org-mode-syntax-table)
  (modify-syntax-entry ?> "." org-mode-syntax-table))

(add-hook 'org-mode-hook #'org-syntax-table-modify)

(require 'org-tempo)
(add-to-list 'org-modules 'org-tempo t)
(add-to-list 'org-structure-template-alist '("sh" . "src sh"))
(add-to-list 'org-structure-template-alist '("lisp" . "src lisp"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("ts" . "src typescript"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("go" . "src go"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))

;; babel
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (awk . t)
   (C . t)
   (css . t)
   (calc . t)
   (screen . t)
   (dot . t )
   (haskell . t)
   (java . t)
   (js . t)
   (latex . t)
   (lisp . t)
   (lua . t)
   (org . t)
   (perl . t)
   (python .t)
   (ruby . t)
   (shell . t)
   (sed . t)
   (scheme . t)
   (sql . t)
   (sqlite . t)))

(setq org-hide-leading-stars t
      org-show-notification-handler "notify-send"
      org-catch-invisible-edits 'smart
      org-archive-subtree-save-file-p t
      org-use-property-inheritance t
      org-startup-folded nil
      org-indirect-buffer-display 'current-window
      org-eldoc-breadcrumb-separator " → "
      org-enforce-todo-dependencies t
      org-entities-user
      '(("flat"  "\\flat" nil "" "" "266D" "♭")
        ("sharp" "\\sharp" nil "" "" "266F" "♯"))
      org-fontify-done-headline t
      org-fontify-quote-and-verse-blocks t
      org-fontify-whole-heading-line t
      org-image-actual-width nil
      org-imenu-depth 6
      org-priority-faces
      '((?A . error)
        (?B . warning)
        (?C . success))
      org-startup-indented nil
      org-adapt-indentation t
      org-tags-column 0
      org-use-sub-superscripts '{})

(add-to-list '+diminished-list 'org-indent-mode)
(customize-set-variable 'org-list-allow-alphabetical t)
(customize-set-variable 'org-preview-latex-image-directory
                        (expand-file-name "org/ltximg/" +emacs-data-dir))

;; +org-archive-file
(defvar +org-archives-file "archives.org"
  "Default target for storing archived entries.
      Is relative to `org-directory', unless it is absolute.")

(defun +org-archives-file ()
  "Expand `+org-archives-file' from `org-directory'.
      If it is an absolute path return `+org-archives-file' verbatim."
  (expand-file-name +org-archives-file org-directory))

;; +org-capture-todo-file
(defvar +org-capture-todo-file "todo.org"
  "Default target for todo entries.
                    Relative to `org-directory', unless it is absolute.")

(defun +org-capture-todo-file ()
  "Expand `+org-capture-todo-file' from `org-directory'.
                    If it is an absolute path return `+org-capture-todo-file' verbatim."
  (expand-file-name +org-capture-todo-file org-directory))

;; +org-capture-notes-file
(defvar +org-capture-notes-file "notes.org"
  "Default target for storing notes.
      Used as a fall back file for org-capture.el, for templates that do not specify a
      target file. Is relative to `org-directory', unless it is absolute.")

(defun +org-capture-notes-file ()
  "Expand `+org-capture-notes-file' from `org-directory'.
      If it is an absolute path return `+org-capture-notes-file' verbatim."
  (expand-file-name +org-capture-notes-file org-directory))

;; +org-capture-links-file
(defvar +org-capture-links-file "links.org"
  "Default target for storing timestamped journal entries.
      Is relative to `org-directory', unless it is absolute.")

(defun +org-capture-links-file ()
  "Expand `+org-capture-links-file' from `org-directory'.
      If it is an absolute path return `+org-capture-links-file' verbatim."
  (expand-file-name +org-capture-links-file org-directory))

;; +org-capture-habits-file
(defvar +org-capture-habits-file "habits.org"
  "Default target for storing repeated entries.
      Is relative to `org-directory', unless it is absolute.")

(defun +org-capture-habits-file ()
  "Expand `+org-capture-habits-file' from `org-directory'.
      If it is an absolute path return `+org-capture-habits-file' verbatim."
  (expand-file-name +org-capture-habits-file org-directory))

;; +org-capture-projects-file
(defvar +org-capture-projects-file "projects.org"
  "Default target for storing project entries.
      Is relative to `org-directory', unless it is absolute.")

(defun +org-capture-projects-file ()
  "Expand `+org-capture-projects-file' from `org-directory'.
      If it is an absolute path return `+org-capture-projects-file' verbatim."
  (expand-file-name +org-capture-projects-file org-directory))

;; org-capture
;; tasks
(setq org-capture-templates
      '(("t" "Personal (t)Tasks")
        ("tt" "(t)todo" entry
         (file+headline +org-capture-todo-file "Inbox")
         "** TODO [#A] %? %^G:followup:\n:PROPERTIES:\n:Via: %a\n:Note:\n:END:\n:LOGBOOK:\n- State \"TODO\"\tfrom\t\"\"\t%U\n:END:"
         :empty-line 1
         :clock-in t
         :clock-resume t
         :jump-to-captured t)
        ("ts" "(p)In Progress Tasks" entry
         (file+headline +org-capture-todo-file "Inbox")
         "** PROG [#B] %? %^G:followup:\n:PROPERTIES:\n:Via: %a\n:Note:\n:END:\n:LOGBOOK:\n- State \"PROG\"\tfrom\t\"\"\t%U\n:END:"
         :empty-line 1
         :clock-in t
         :clock-resume t
         :jump-to-captured t)
        ("tn" "(n)Notes" entry
         (file+headline +org-capture-notes-file "Notes")
         "** %? %^G\n:PROPERTIES:\n:Via: %a\n:Note:\n:END:\n"
         :empty-line 1
         :jump-to-captured t)
        ("te" "(e)Next" entry
         (file+headline +org-capture-todo-file "Inbox")
         "** NEXT %^G\n:PROPERTIES:\n:Via: %a\n:Note:\n:END:\n"
         :empty-line 1
         :jump-to-captured t)
        ("ti" "(I)Ideas" entry
         (file+headline +org-capture-notes-file "Ideas")
         "** MAYBE %^G\n:PROPERTIES:\n:Via: %a\n:Note:\n:END:\n"
         :empty-line 1
         :jump-to-captured t)
        ("th" "(h)Habit" entry
         (file+headline +org-capture-habits-file "Recurring")
         "** TODO [#C] %? %^G:habit:\n SCHEDULED: %^t\n:PROPERTIES:\n:Style: habit\n:Note:\n:END:\n:LOGBOOK:\n- State \"TODO\"\tfrom \"\"\t%U\n:END:"
         :empty-line 1
         :jump-to-captured t)
        ("td" "(d)Done" entry
         (file+headline +org-capture-notes-file "Inbox")
         "** DONE %?\nCLOSED: %U\n:PROPERTIES:\n:Via: %a\n:Note:\n:END:\n:LOGBOOK:\n- State \"DONE\"\tfrom \"\"\t%U\n:END:"
         :empty-lines 1
         :jump-to-captured t)
        ("o" "(o)ther")
        ("ol" "ur(l)" entry
         (file+headline +org-capture-links-file "Links")
         "** %a\n:PROPERTIES:\n:Via: %l\n:END:\n\n %?")))

(add-to-list '+diminished-list 'org-indent-mode)

;; dired
(with-eval-after-load 'dired
  (setq-default dired-listing-switches "-alh"
                auto-revert-verbose t
                auto-revert-use-notify nil
                auto-revert-stop-on-user-input nil
                revert-without-query (list ".")
                image-dired-db-file (expand-file-name "image-dired/db.el" +emacs-data-dir)
                image-dired-dir (expand-file-name "image-dired/" +emacs-data-dir)
                image-dired-gallery-dir (expand-file-name "image-dired/gallery/" +emacs-data-dir)
                image-dired-temp-image-file (expand-file-name "image-dired/temp-image" +emacs-data-dir)
                image-dired-temp-rotate-image-file (expand-file-name "image-dired/temp-rotate-image" +emacs-data-dir)
                dired-dwim-target t
                dired-hide-details-hide-symlink-targets nil
                dired-auto-revert-buffer #'dired-buffer-stale-p
                dired-recursive-copies  'always
                dired-recursive-deletes 'top
                dired-create-destination-dirs 'ask)
  (add-hook 'dired-load-hook
            (function (lambda () (load "dired-x"))))
  (customize-set-variable 'global-auto-revert-non-file-buffers t))

;; uniquify
(with-eval-after-load 'uniquify
  (setq uniquify-buffer-name-style 'post-forward-angle-brackets))

;; desktop
(with-eval-after-load 'desktop
  (setq desktop-load-locked-desktop t))

;; quicklisp
(if (file-directory-p
     (expand-file-name
      ".ros/lisp/quicklisp"
      (getenv "HOME")))
    (defvar +quicklisp-path
      (expand-file-name
       ".ros/lisp/quicklisp"
       (getenv "HOME")))
  (when (file-directory-p
         (expand-file-name
          ".local/share/quicklisp"
          (getenv "HOME")))
    (defvar +quicklisp-path
      (expand-file-name
       ".local/share/quicklisp"
       (getenv "HOME")))))

;; roswell
(cond
 ((and (executable-find "ros")
       (file-exists-p
        (expand-file-name
         ".roswell/helper.el"
         (getenv "HOME"))))
  (progn
    (load
     (expand-file-name
      ".roswell/helper.el"
      (getenv "HOME")))
    (setq inferior-lisp-program "ros -Q -l ~/.sbclrc run")))
 ((executable-find "sbcl")
  (setq inferior-lisp-program "sbcl")))

;; tramp
(with-eval-after-load 'tramp
  (setq tramp-remote-path
        (append tramp-remote-path
                '(tramp-own-remote-path))
        tramp-auto-save-directory
        (expand-file-name "tramp/auto-save/" +emacs-data-dir)
        tramp-default-method "ssh"
        tramp-persistency-file-name
        (expand-file-name "tramp/persistency.el" +emacs-data-dir)
        remote-file-name-inhibit-cache 60
        tramp-completion-reread-directory-timeout 60
        tramp-verbose 1
        vc-ignore-dir-regexp
        (format "%s\\|%s\\|%s"
                vc-ignore-dir-regexp
                tramp-file-name-regexp
                "[/\\\\]node_modules")))

;; savehist
(savehist-mode 1)

;; calendar
(with-eval-after-load 'calendar
  (setq diary-file "~/Documents/google-drive/Notes/diary")
  (add-hook 'diary-list-entries-hook 'diary-sort-entries t)
  (setq calendar-intermonth-spacing 1))

;; epg
(if (executable-find "gpg2")
    (customize-set-variable 'epg-gpg-program "gpg2")
  (customize-set-variable 'epg-gpg-program "gpg"))

;; whitespace
(require 'whitespace)
(setq whitespace-line-column nil
      whitespace-style
      '(face indentation tabs tab-mark spaces space-mark newline newline-mark
             trailing lines-tail)
      whitespace-display-mappings
      '((tab-mark ?\t [?› ?\t])
        (newline-mark ?\n [?¬ ?\n])
        (space-mark ?\  [?·] [?.])))
(add-to-list '+diminished-list 'whitespace-mode)

;; borrowed from `doom'
(defun doom-highlight-non-default-indentation-h ()
  "Highlight whitespace at odds with `indent-tabs-mode'.
That is, highlight tabs if `indent-tabs-mode' is `nil', and highlight spaces at
the beginnings of lines if `indent-tabs-mode' is `t'. The purpose is to make
incorrect indentation in the current buffer obvious to you.

Does nothing if `whitespace-mode' or `global-whitespace-mode' is already active
or if the current buffer is read-only or not file-visiting."
  (unless (or (eq major-mode 'fundamental-mode)
              (bound-and-true-p global-whitespace-mode)
              (null buffer-file-name))
    (require 'whitespace)
    (set (make-local-variable 'whitespace-style)
         (cl-union (if indent-tabs-mode
                       '(indentation)
                     '(tabs tab-mark))
                   (when whitespace-mode
                     (remq 'face whitespace-active-style))))
    (cl-pushnew 'face whitespace-style) ; must be first
    (whitespace-mode +1)))
(add-hook 'after-change-major-mode-hook #'doom-highlight-non-default-indentation-h 'append)

;; visual-line-mode
(add-hook 'text-mode-hook #'visual-line-mode)
(add-to-list '+diminished-list 'visual-line-mode)

;; show-paren-mode
(setq show-paren-delay 0.1
      show-paren-highlight-openparen t
      show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t)

;; delete-selection-mode
(delete-selection-mode 1)

(require 'recentf)
(add-to-list 'recentf-exclude "/\\(\\(\\(COMMIT\\|NOTES\\|PULLREQ\\|MERGEREQ\\|TAG\\)_EDIT\\|MERGE_\\|\\)MSG\\|\\(BRANCH\\|EDIT\\)_DESCRIPTION\\)\\'")
(setq recentf-auto-cleanup nil
      recentf-max-saved-items 200)
(add-to-list 'recentf-exclude
             (concat "^" (regexp-quote (or (getenv "XDG_RUNTIME_DIR")
                                           "/run"))))
(add-to-list 'recentf-filename-handlers #'substring-no-properties)
(setq recentf-auto-cleanup (if (daemonp) 300))
(add-hook 'kill-emacs-hook #'recentf-cleanup)

;; Doesn't exist in terminal Emacs, but some Emacs packages (internal and
;; external) use it anyway, leading to a void-function error, so define a no-op
;; substitute to suppress them.
(unless (fboundp 'define-fringe-bitmap)
  (fset 'define-fringe-bitmap #'ignore))

;; flyspell-prog-mode
(add-hook 'prog-mode-hook #'flyspell-prog-mode)
(when (executable-find
       (or "ispell" "aspell")
       (ispell-minor-mode 1)))
(add-to-list '+diminished-list 'ispell-minor-mode)
(add-to-list '+diminished-list 'flyspell-mode)

;; tab-bar
(defun tab-create (name)
  "Create new tab bar with NAME if not already exists."
  (condition-case nil
      (unless (equal
               (alist-get 'name (tab-bar--current-tab))
               name)
        (tab-bar-rename-tab-by-name name))
    (error (tab-new)
           (tab-bar-rename-tab name))))

(setq tab-bar-back-button ""
      tab-bar-forward-button ""
      tab-bar-close-button ""
      tab-bar-new-button "")

;; https://christiantietze.de/posts/2022/02/emacs-tab-bar-numbered-tabs/
(defvar ct/circle-numbers-alist
  '((0 . "⓪")
    (1 . "①")
    (2 . "②")
    (3 . "③")
    (4 . "④")
    (5 . "⑤")
    (6 . "⑥")
    (7 . "⑦")
    (8 . "⑧")
    (9 . "⑨"))
  "Alist of integers to strings of circled unicode numbers.")

(defun ct/tab-bar-tab-name-format-default (tab i)
  (let ((current-p (eq (car tab) 'current-tab))
        (tab-num (if (and tab-bar-tab-hints (< i 10))
                     (alist-get i ct/circle-numbers-alist) "")))
    (propertize
     (concat tab-num
             " "
             (alist-get 'name tab)
             (or (and tab-bar-close-button-show
                      (not (eq tab-bar-close-button-show
                               (if current-p 'non-selected 'selected)))
                      tab-bar-close-button)
                 "")
             " ")
     'face (funcall tab-bar-tab-face-function tab))))
(setq tab-bar-tab-name-format-function #'ct/tab-bar-tab-name-format-default)

(when +tab-bar-enabled
  (customize-set-variable 'tab-bar-show nil)
  (tab-bar-mode 1)
  (tab-bar-history-mode 1))

(when +savehist-enabled
  (setq savehist-save-minibuffer-history t
        savehist-autosave-interval nil
        savehist-additional-variables
        '(kill-ring                        ; persist clipboard
          register-alist                   ; persist macros
          mark-ring global-mark-ring       ; persist marks
          search-ring regexp-search-ring))
  (require 'savehist))

(when +saveplace-enabled
  (require 'saveplace)
  (save-place-mode 1))

;; Extra file extensions to support
(nconc
 auto-mode-alist
 '(("/LICENSE\\'" . text-mode)
   ("\\.log\\'" . text-mode)
   ("rc\\'" . conf-mode)
   ("\\.\\(?:hex\\|nes\\)\\'" . hexl-mode)))

(require 'recentf)
(recentf-mode 1)

(defun +update-font-and-diminish ()
  "Update font and diminished modes."
  (progn
    (cond
     ((find-font (font-spec :name "Ubuntu Mono"))
      (set-frame-font "Ubuntu Mono-10"))
     ((find-font (font-spec :name "Fira Code"))
      (set-frame-font "Fira Code-10"))
     ((find-font (font-spec :name "Source Code Pro"))
      (set-frame-font "Source Code Pro-10"))
     ((find-font (font-spec :name "DejaVu Sans Mono"))
      (set-frame-font "DejaVu Sans Mono-10")))
    (when (featurep 'diminish)
      (dolist (mode +diminished-list)
        (diminish mode)))))

(add-hook 'after-init-hook #'+update-font-and-diminish)
(add-hook 'server-after-make-frame-hook #'+update-font-and-diminish)
(add-hook 'before-make-frame-hook #'+update-font-and-diminish)

(provide 'sane-defaults)
;;; sane-defaults.el ends here
